﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Practical_8
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<Room> roomList = new List<Room>();
        List<Patient> patientList = new List<Patient>();
        List<Doctor> doctorList = new List<Doctor>();

        public MainPage()
        {
            this.InitializeComponent();

            InitData();
        }

        private void InitData()
        {
            roomList.Add(new Room("#01-01", "C", 52));
            roomList.Add(new Room("#02-02", "B", 80));
            roomList.Add(new Room("#03-03", "A", 140));

            doctorList.Add(new Doctor("S1234567A", "Tom", "Pediatrics"));
            doctorList.Add(new Doctor("S2345678A", "Champ", "Oncology"));
            doctorList.Add(new Doctor("S3456789B", "Terry", "Cardiology"));

            foreach (Doctor d in doctorList)
            {
                d.PatientList = new List<Patient>();
            }

            lvRooms.ItemsSource = roomList;
            lvPatients.ItemsSource = patientList;
            lvDoctors.ItemsSource = doctorList;
        }

        private void btnCreatePatient_Click(object sender, RoutedEventArgs e)
        {
            Patient p = new Patient(roomList[lvRooms.SelectedIndex]);
            p.Nric = txtNRIC.Text;
            p.Name = txtName.Text;

            patientList.Add(p);

            RefreshListView(lvPatients, patientList);

        }

        private void RefreshListView(ListView lv, object ls)
        {
            lv.ItemsSource = null;
            lv.ItemsSource = ls;
        }

        private void lvDoctors_ItemClick(object sender, ItemClickEventArgs e)
        {
                Doctor d = (Doctor)e.ClickedItem;
                RefreshListView(lvPatientsUnderDoctor, d.PatientList);
        }

        private void btnAssignPatient_Click(object sender, RoutedEventArgs e)
        {
            doctorList[lvDoctors.SelectedIndex].AddPatient(patientList[lvPatients.SelectedIndex]);

            RefreshListView(lvPatientsUnderDoctor, doctorList[lvDoctors.SelectedIndex].PatientList);

        }

        private void btnRemovePatient_Click(object sender, RoutedEventArgs e)
        {
            doctorList[lvDoctors.SelectedIndex].RemovePatient(lvPatientsUnderDoctor.SelectedIndex);
            RefreshListView(lvPatientsUnderDoctor, doctorList[lvDoctors.SelectedIndex].PatientList);
        }

        private void btnCalculateBill_Click(object sender, RoutedEventArgs e)
        {
            if (lvPatients.SelectedIndex >= 0)
            {
                patientList[lvPatients.SelectedIndex].CalculateCharges();
                RefreshListView(lvPatients, patientList);
            }
        }
    }
}
