﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_8
{
    class Doctor:Person
    {
        private string department;

        public string Department
        {
            get { return department; }
            set { department = value; }
        }

        private List<Patient> patientList;

        public List<Patient> PatientList
        {
            get { return patientList; }
            set { patientList = value; }
        }

        public Doctor(string ic, string nm, string dep):base(ic,nm)
        {
            Department = dep;
        }

        public void AddPatient(Patient p)
        {
            PatientList.Add(p);
        }

        public void RemovePatient(int i)
        {
            PatientList.RemoveAt(i);
        }

        public override string ToString()
        {
            return base.ToString() + $" {Department}";
        }

    }
}
