﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_8
{
    class Patient:Person
    {
        private Room wardedAt;

        public Room WardedAt
        {
            get { return wardedAt; }
            set { wardedAt = value; }
        }

        private double billPayable;

        public double BillPayable
        {
            get { return billPayable; }
            set { billPayable = value; }
        }

        public Patient(Room r)
        {
            WardedAt = r;
        }

        public void CalculateCharges()
        {
            BillPayable += WardedAt.CostPerNight;
        }

        public override string ToString()
        {
            return base.ToString() + $" {WardedAt} ${BillPayable}";
        }

    }
}
