﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_8
{
    class Person
    {
        private string nric;

        public string Nric
        {
            get { return nric; }
            set { nric = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Person()
        {

        }

        public Person(string ic, string nm)
        {
            Nric = ic;
            Name = nm;
        }

        public override string ToString()
        {
            return $"{Name} ({Nric})";
        }

    }
}
