﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical_8
{
    class Room
    {
        private string location;

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        private string wardClass;

        public string WardClass
        {
            get { return wardClass; }
            set { wardClass = value; }
        }

        private int costPerNight;

        public int CostPerNight
        {
            get { return costPerNight; }
            set { costPerNight = value; }
        }

        public Room(string loc, string ward, int cost)
        {
            Location = loc;
            WardClass = ward;
            CostPerNight = cost;
        }

        public override string ToString()
        {
            return $"{Location} {WardClass} ${CostPerNight}/night";
        }
    }
}
